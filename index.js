#! /usr/bin/env node

const puppeteer = require('puppeteer');
const stats = require('stats-lite')
const program = require('commander');

const DEFAULT_TEST_PAGE = 'http://localhost:8000';
const DEFAULT_NUM_TESTS = 10;

program
    .option('-n, --number <n>', 'Number of tests', parseInt)
    .option('-u, --url [String]', 'URL to test', String)
    .parse(process.argv);

const testPage = program.url || DEFAULT_TEST_PAGE;
const numTests = program.number || DEFAULT_NUM_TESTS;

console.log('GOGOGO');

const dcl = [];
const load = [];

const runTest = async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const results = {
        domcontentloaded: null,
        load: null,
    }

    let start;

    //page.on('metrics', console.log);
    page.on('domcontentloaded', () => {results.domcontentloaded = (new Date()) - start;});
    page.on('load', () => {results.load = (new Date()) - start;});

    start = new Date();
    await page.goto(testPage);
    await browser.close();
    dcl.push(results.domcontentloaded);
    load.push(results.load);
};

const runManyTests = async (num) => {
    for (i = 0; i < num; i++) {
        console.log(`Test ${i + 1} of ${num}`);
        await runTest();
    }
}

runManyTests(numTests).then(() => {
    console.log('done');
    console.log(`Average time for domcontentloaded was ${stats.mean(dcl)}ms with standard deviation ${stats.stdev(dcl)}ms`);
    console.log(`Average time for load was ${stats.mean(load)}ms with standard deviation ${stats.stdev(load)}ms`);
    process.exit();
});